echo "Hydronium energy conservation"
python PIMD.py --temp 300 --numBeads 64 --numSteps 100 --fs 0.1 -P 'hydronium' --thermostat 'None' --printEne 1 --time --PIMDtype 'RP' --eneFile "eneFile2.csv" --eneEst 'Classical Energy'

