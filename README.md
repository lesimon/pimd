# PIMD code with normal mode propagator

### Installing

Just clone this repository in the same directory as polylib or in a directory where it has access to polylib.

### Files

* cyclic_NM.py: Calculates eigenvalues of Hessian and performs normal mode transformations.
* energyEstimator.py: Functions for calculating the energy of a system with energy estimators.
* FEP.py: Python script for simplifying FEP calculations.
* NMpropagator.py: Contains code for the normal mode propagator.
* PIMD.py: The main program which takes in arguments such as '--numSteps 1000' and performs a PIMD simulation.
* RPpropagator.py: Contains code for the ring-polymer propagator.
* sample.sh, sample2.sh, sample3.sh: Examples on how to use PIMD.py
* test.py: Small test script to check whether there are no obvious bugs in the code.

