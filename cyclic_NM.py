#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: simon

Classes for normal mode transformations derived from the
Hessian.
"""

from __future__ import division, print_function
import numpy as np
from builtins import object


class cyclic(object):
    r"""
    Performs normal mode transformations for a cyclic ring-
    polymer.
    """
    def __init__(self, numBeads):
        r"""
        Initialise the normal mode tranfromation for the cyclic
        ring polymer.

        Parameters
        ----------
        numBeads : integer
            The number of beads of the system, for which the
            normal mode transformation is applied.

        Returns
        -------
        None

        """
        self.n = numBeads
        self.nmm, self.eigenvalues = self.setNormalModeMatrix()
        # nmm = normal mode transformation matrix

        # Sometimes when an eigenvalue is zero, it returns a
        # negative eigenvalue like -5.284e-16 or -7.546e-16 instead
        # of returning zero. This causes an error when taking
        # np.sqrt() of eigenvalues later. The next 2 lines provide
        # a temporary fix for this issue.
        if abs(self.eigenvalues[0]) < 1.0e-15:
            self.eigenvalues[0] = 0
        self.nmmt = np.transpose(self.nmm)  # nmmt = transposed nmm

    def setNormalModeMatrix(self):
        Hessian = np.zeros((self.n, self.n), float)
        np.fill_diagonal(Hessian, 2.)
        Hessian -= ((np.roll(Hessian, -1, axis=1) +
                     np.roll(Hessian, 1, axis=1))/2)
        eigenvalues, NormalModeMatrix = np.linalg.eigh(Hessian)
        # eigenvalues are already sorted in ascending order
        return NormalModeMatrix, eigenvalues

    r"""
    Performs the normal mode transformtion.

    Parameters
    ----------
    x : ndarray, shape(N, ...)
        The position or momentum in mass-weighted coordinates to
        transform.

    Returns
    -------
    y : ndarray, shape(x.shape)
        y in normal mode coordinates after applying the
        inverse normal mode transformation.
    """
    def forward(self, x):
        # matrix multiplication with first column of x
        return np.einsum('ij, j...->i...', self.nmmt, x)

    r"""
    Performs the inverse normal mode transformtion.

    Parameters
    ----------
    x : ndarray, shape(N, ...)
        The position or momentum in mass-weighted normal mode
        coordinates to transform.

    Returns
    -------
    y : ndarray, shape(x.shape)
        y in position coordinates after applying the
        inverse normal mode transformation.
    """
    def backward(self, x):
        # matrix multiplication of nmm with first column of x.
        return np.einsum('ij, j...->i...', self.nmm, x)


class open_cyclic(cyclic):
    r"""
    Performs normal mode transformations for a cyclic ring-
    polymer, where the Hessian is modified in a way, that the
    spring between the first and last bead is an inverse spring.

    :math:`\sum _{i=1}^{N-1} \left( \mathbf{x_i} - \mathbf{x_{i+1}}\right)
    + (1-\xi) \left( \mathbf{x_N} - \mathbf{x_1} \right)
    + \xi \left( \mathbf{x_N} + \mathbf{x_1} \right)`

    For Xi = 0.0, it is identical to the cyclic class.
    """
    def __init__(self, numBeads, Xi):
        self.Xi = Xi
        super(open_cyclic, self).__init__(numBeads)

    def setNormalModeMatrix(self):
        Hessian = np.zeros((self.n, self.n), float)
        np.fill_diagonal(Hessian, 2.)
        Hessian -= ((np.roll(Hessian, -1, axis=1) +
                     np.roll(Hessian, 1, axis=1))/2)
        Hessian[0, self.n-1] += 2*self.Xi
        Hessian[self.n-1, 0] += 2*self.Xi
        eigenvalues, NormalModeMatrix = np.linalg.eigh(Hessian)
        return NormalModeMatrix, eigenvalues

