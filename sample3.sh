echo "Double well free energy perturbation calculation"
for rand in 0 1
do
    FEPfile="FEPfile_rand${rand}.txt"
    python PIMD.py --beta 2 --numBeads 48 --numSteps 10000 --dt 0.021 -P 'doubleWell' --thermostat 'Andersen' --fep 1 --fepFile $FEPfile --eqSteps 2000 --seed $rand --Xi 0.5
    python FEP.py --stepsize 0.1 --input $FEPfile
done
