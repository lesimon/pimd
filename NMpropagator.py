#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: simon

Various propagators for PIMD simulation using normal modes.
The equations used here are taken from DOI: 10.1063/1.3489925
The number in the comments indicates the equations from
DOI: 10.1063/1.3489925.
"""

from __future__ import division, print_function
import math
import numpy as np
import random
from polylib import md
from polylib import util
import cyclic_NM

# TODO: Reintroduce support for imaginary omegak


class NMpropagator(md.vv):
    r"""
    The unthermostatted PIMD normal mode propagator.
    """
    def __init__(self, PES, m, dt, numBeads, beta, nm, hbar=1):
        r"""
        Initialise the NMpropagator

        Parameters
        ----------
        PES : PES object (has member functions such as force() and potential()
            defined.)
            The PES on which a PIMD simulation is performed.

        m : float or ndarray of shape (M,) or (M, N)
            Atomic masses.

        dt : float
            timestep

        numBeads : integer
            number of beads

        beta : float
            beta = 1/kT

        nm: Normal mode object, for which following members are defined:
            nm.forward(x): performs a normal mode tranformation on x.
            nm.backward(x): performs an inverse normal mode tranformation on x.
            nm.eigenvalues: The eigenvalues of the Hessian, which is used as a
                            basis for the normal mode transformation.

        hbar : float, OPTIONAL
            hbar. Default 1.

        Returns
        -------
        None

        """
        super(NMpropagator, self).__init__(PES, m, dt)
        self.sqrtm = np.sqrt(m)
        self.n = numBeads
        self.beta = beta
        self.betaN = beta/numBeads
        self.hbar = hbar
        self.omegan = 1./self.betaN/hbar
        self.nm = nm
        self.omegak = self.omegan * np.sqrt(self.nm.eigenvalues)
        self.N = np.where(np.isscalar(m), 1,
                          np.sum(np.ones(np.array(m).shape)))
        self.c1 = np.cos(self.omegak*self.dt)
        self.c2 = self.omegak*np.sin(self.omegak*self.dt)
        self.c3 = np.zeros(self.n)
        self.c3[0] = self.dt
        for k in range(1, numBeads):
            self.c3[k] = (1./self.omegak[k])*np.sin(self.omegak[k]*self.dt)

    def ffun(self):
        self.Epot, self.f = self.PES.both(self.x)
        self.f *= -1

    def step1(self, s=1):
        self.c_1 = util.backwardsbroadcast(self.c1, self.x)
        self.c_2 = util.backwardsbroadcast(self.c2, self.x)
        self.c_3 = util.backwardsbroadcast(self.c3, self.x)
        self.p += s * self.dt * self.f / 2  # (21)
        self.p /= self.sqrtm  # mass-weighting
        self.x *= self.sqrtm
        p_nm = self.nm.forward(self.p)  # (22)
        x_nm = self.nm.forward(self.x)  # (22)
        p_nmCopy = np.copy(p_nm)  # (23)
        x_nmCopy = np.copy(x_nm)  # (23)
        p_nm[:] = self.c_1*p_nm - self.c_2*x_nm  # (23)
        x_nm[:] = self.c_1*x_nm + self.c_3*p_nmCopy  # (23)
        self.p = self.nm.backward(p_nm)  # (24)
        self.x = self.nm.backward(x_nm)  # (24)
        self.p *= self.sqrtm  # mass-deweighting
        self.x /= self.sqrtm


class Andersen(NMpropagator):
    r"""
    PIMD propagator with Andersen thermostat. nu describes the probability
    with which momenta are resampled.
    """
    def __init__(self, PES, mass, dt, numBeads, beta, nu, nm, hbar=1.):
        r"""
        Initialise the Andersen normal mode propagator

        Parameters
        ----------
        PES : PES object (has member functions such as force() and potential()
            defined.)
            The PES on which a PIMD simulation is performed.

        m : float or ndarray of shape (M,) or (M, N)
            Atomic masses.

        dt : float
            timestep

        numBeads : integer
            number of beads

        beta : float
            beta = 1/kT

        nu : float with a value between 0.0 and 1.0.
            Probability, with which momenta are resampled during every
            timestep.

        nm: Normal mode object, for which following members are defined:
            nm.forward(x): performs a normal mode tranformation on x.
            nm.backward(x): performs an inverse normal mode tranformation on x.
            nm.eigenvalues: The eigenvalues of the Hessian, which is used as a
                            basis for the normal mode transformation.

        hbar : float, OPTIONAL
            hbar. Default 1.

        Returns
        -------
        None

        """
        super(Andersen, self).__init__(PES, mass, dt, numBeads, beta=beta,
                                       nm=nm, hbar=hbar)
        self.nu = nu
        self.SD = np.sqrt(self.m/self.betaN)

    def rescale(self, definitely=False):
        if definitely or random.random() < self.nu:
            self.p = np.random.normal(0, self.SD, self.p.shape)

    def step2(self, s=1):
        NMpropagator.step2(self, s)  # (25)
        self.rescale()


class Langevin(NMpropagator):
    r"""
    PIMD propagator with Langevin thermostat according to
    DOI: 10.1063/1.3489925. tau describes the coupling parameter for the
    lowest energy normal mode.
    """
    def __init__(self, PES, mass, dt, numBeads, beta, tau, nm, hbar=1.):
        r"""
        Initialise the Langevin thermostatted normal mode propagator.

        Parameters
        ----------
        PES : PES object (has member functions such as force() and potential()
            defined.)
            The PES on which a PIMD simulation is performed.

        m : float or ndarray of shape (M,) or (M, N)
            Atomic masses.

        dt : float
            timestep

        numBeads : integer
            number of beads

        beta : float
            beta = 1/kT

        tau : float
            Inverse coupling strength of thermostat to centroid normal mode.

        nm: Normal mode object, for which following members are defined:
            nm.forward(x): performs a normal mode tranformation on x.
            nm.backward(x): performs an inverse normal mode tranformation on x.
            nm.eigenvalues: The eigenvalues of the Hessian, which is used as a
                            basis for the normal mode transformation.

        hbar : float, OPTIONAL
            hbar. Default 1.

        Returns
        -------
        None

        """
        super(Langevin, self).__init__(PES, mass, dt, numBeads, beta=beta,
                                       nm=nm, hbar=hbar)
        self.tau = tau
        self.gamma = np.where(self.omegak < 0.01, 1./tau, 2*self.omegak)
        self.c4 = np.exp(-dt/2.*self.gamma)
        self.c5 = np.sqrt(1-self.c4**2)

    def step1(self, s=1):
        self.c_4 = util.backwardsbroadcast(self.c4, self.x)
        self.c_5 = util.backwardsbroadcast(self.c5, self.x)
        p_nm = self.nm.forward(self.p)  # (27)
        p_nm[:] = (self.c_4*p_nm + np.sqrt(self.m/self.betaN) * self.c_5 *
                   np.random.normal(0.0, 1.0, size=np.shape(p_nm)))  # (28)
        self.p = self.nm.backward(p_nm)  # (29)
        NMpropagator.step1(self, s)  # (21, 22, 23, 24)

    def step2(self, s=1):
        self.c_4 = util.backwardsbroadcast(self.c4, self.x)
        self.c_5 = util.backwardsbroadcast(self.c5, self.x)
        NMpropagator.step2(self, s)  # (25)
        p_nm = self.nm.forward(self.p)  # (27)
        p_nm[:] = (self.c_4*p_nm + np.sqrt(self.m/self.betaN) * self.c_5 *
                   np.random.normal(0.0, 1.0, size=np.shape(p_nm)))  # (28)
        self.p = self.nm.backward(p_nm)  # (29)

