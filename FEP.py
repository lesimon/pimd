#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 18 13:00 2021

@author: simon

Program to calculate partition function ratios for free energy perturbation.
Example of usage: python FEP.py --stepsize 0.1 --input FEPresults.csv
"""

from __future__ import division, print_function
import argparse
import numpy as np

parser = argparse.ArgumentParser(add_help=True)
parser.add_argument('--input', default=None, help="Name of input file, from which ratios of partition functions are to be calculated.")
parser.add_argument('--stepsize', default=0.1, type=float, help="stepsize used for calculating intermediate partition function ratios.")

if __name__ == "__main__":
    args = parser.parse_args()
    if args.input is not None:
        c = np.loadtxt(args.input)
    else:
        raise ValueError("Filename is empty!")
    ZbyZ = np.mean(np.exp(args.stepsize*c))
    print("For stepsize " + str(args.stepsize) + ", Z-ratio is equal to:")
    print(ZbyZ)
    

