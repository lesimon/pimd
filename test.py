from __future__ import print_function, division
import matplotlib.pyplot as plt
import numpy as np

from polylib.PES import oneD
from polylib.PES import rp
import cyclic_NM
import NMpropagator as nmp
import RPpropagator as rpp

if __name__ == "__main__":
    m = 1.
    numBeads = 16
    pos = np.zeros(numBeads)+0.2
    mom = np.zeros(numBeads)
    pos[5] = 0.4
    pos[10] = 0.1
    w = 1.
    beta = 1.
    dt = 0.001
    numSteps = 1000
    Xi = 0.0  # Only works for Xi=0
    hbar = 1.

    potential = oneD.quartic(m, w)

    # Normal mode propagator
    nm = cyclic_NM.open_cyclic(numBeads, Xi)
    propagator = nmp.NMpropagator(potential, m, dt, numBeads,
                                  beta=beta, nm=nm, hbar=hbar)
    xt, pt = propagator.propagate(pos, mom, numSteps)

    # Ring polymer propagator
    rp_propagator = rpp.RPpropagator(potential, m, dt, numBeads,
                                     beta=beta, Xi=Xi, MTSfactor=1,
                                     forceType=1, hbar=hbar)
    rp_propagator.set_xp(pos, mom)
    xt2 = np.zeros(xt.shape)
    xt2[0] = pos
    pt2 = np.zeros(pt.shape)
    pt2[0] = mom
    for i in range(numSteps):
        rp_propagator.step()
        xt2[i+1] = rp_propagator.x.copy()
        pt2[i+1] = rp_propagator.p.copy()

    xt = xt.mean(axis=tuple(range(1, xt.ndim)))
    xt2 = xt2.mean(axis=tuple(range(1, xt2.ndim)))
    diff = np.mean(abs(xt-xt2))
    title = np.where(diff < 1e-5, "Success", "Fail")
    print("Printing xt: ")
    print(xt)
    print("Printing xt2:")
    print(xt2)
    plt.figure
    plt.plot(xt, 'r-', label='NMpropagator code')
    plt.plot(xt2, 'g-', label='RPpropagator code')
    plt.legend()
    plt.title(title)
    plt.show()
