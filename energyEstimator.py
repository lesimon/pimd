import numpy as np
from polylib import eckart_frame

# beta unused here. Think about removing it.
def virialEstimator(pos, mom, m, potential, force, numBeads, beta, Xi, hbar):
    return (np.sum(potential) - np.sum(pos/2 * force))/numBeads

def centroidVirialEstimator(pos, mom, m, potential, force, numBeads, beta, Xi, hbar):
    N = np.sum(np.ones(pos[0].shape))
    return np.sum(potential)/numBeads - np.sum((pos-np.mean(pos, axis=0))*force)/numBeads/2 + N/2/beta

def classical(pos, mom, m, potential, force, numBeads, beta, Xi, hbar):
    # closed ring polymer
    # Can be used to check if energy is conserved.
    omegan = numBeads/(beta*hbar)
    beadpotential = m*np.square(omegan)/2 * np.square(pos-np.roll(pos, 1, axis=0))
    beadpotential = np.sum(beadpotential.reshape(beadpotential.shape[0], -1), axis=(1))
    #Eck = eckart_frame.EckartFrame(pos[-1], m.flatten())
    #xrot, rotmat, norm2, jac = Eck.norm2deriv(pos[0])
    #Eck2 = eckart_frame.EckartFrame(-pos[-1], m.flatten())
    #xrot2, rotmat2, norm22, jac2 = Eck2.norm2deriv(pos[0])
    #beadpotential[0] = omegan**2/2 * ((1-Xi)*norm2 + Xi*norm22)
    
    #beadpotential[0] = omegan**2/2*np.sum(m*np.square(Eck.xe - xrot))
    

    beadpotential[0] = np.sum(m*np.square(omegan)/2 * ((1-Xi)*np.square(pos[0]-pos[-1]) + Xi*np.square(pos[0]+pos[-1])))
    return np.sum(mom**2/2/m) + np.sum(potential) + np.sum(beadpotential)

def classicalH2O(pos, mom, m, potential, force, numBeads, beta, Xi, hbar):
    # closed ring polymer
    # Can be used to check if energy is conserved.
    omegan = numBeads/(beta*hbar)
    beadpotential = m*np.square(omegan)/2 * np.square(pos-np.roll(pos, 1, axis=0))
    beadpotential = np.sum(beadpotential.reshape(beadpotential.shape[0], -1), axis=(1))
    Eck = eckart_frame.EckartFrame(pos[-1], m.flatten())
    xrot, rotmat, norm2, jac = Eck.norm2deriv(pos[0])
    #Eck2 = eckart_frame.EckartFrame(-pos[-1], m.flatten())
    #xrot2, rotmat2, norm22, jac2 = Eck2.norm2deriv(pos[0])
    #beadpotential[0] = omegan**2/2 * ((1-Xi)*norm2 + Xi*norm22)
    
    #beadpotential[0] = omegan**2/2*np.sum(m*np.square(Eck.xe - xrot))
   
    beadpotential[0] = omegan**2/2 * (np.sum(m*(1-Xi)*np.square(pos[0]-pos[-1])) + Xi*norm2)
    #beadpotential[0] = np.sum(m*np.square(omegan)/2 * ((1-Xi)*np.square(pos[0]-pos[-1]) + Xi*np.square(pos[0]+pos[-1])))
    return np.sum(mom**2/2/m) + np.sum(potential) + np.sum(beadpotential)


def classicalH3O(pos, mom, m, potential, force, numBeads, beta, Xi, hbar):
    # closed ring polymer
    # Can be used to check if energy is conserved.
    omegan = numBeads/(beta*hbar)
    beadpotential = m*np.square(omegan)/2 * np.square(pos-np.roll(pos, 1, axis=0))
    beadpotential = np.sum(beadpotential.reshape(beadpotential.shape[0], -1), axis=(1))
    Eck = eckart_frame.EckartFrame(pos[-1], m.flatten())
    xrot, rotmat, norm2, jac = Eck.norm2deriv(pos[0])
    Eck2 = eckart_frame.EckartFrame(-pos[-1], m.flatten())
    xrot2, rotmat2, norm22, jac2 = Eck2.norm2deriv(pos[0])
    beadpotential[0] = omegan**2/2 * ((1-Xi)*norm2 + Xi*norm22)
    
    #print(beadpotential[0])
    #beadpotential[0] = omegan**2/2*np.sum(m*np.square(Eck.xe - xrot))
    #print(beadpotential[0])
    
    #beadpotential[0] = m*np.square(omegan)/2 * ((1-Xi)*np.square(pos[0]-pos[-1]) + Xi*np.square(pos[0]+pos[-1]))
    return np.sum(mom**2/2/m) + np.sum(potential) + np.sum(beadpotential)


def classical2(pos, mom, m, potential, force, numBeads, beta):
    # closed ring polymer
    # Can be used to check if energy is conserved.
    hbar = 1. # for now...
    omegan = numBeads/(beta*hbar)
    beadpotential = m*np.square(omegan)/2 * np.square(pos-np.roll(pos, 1, axis=0))
    beadpotential = np.sum(beadpotential, axis=(1,2))
    Eck = eckart_frame.EckartFrame(pos[-1], m.flatten())
    xrot, rotmat, norm2, jac = Eck.norm2deriv(pos[0])
    beadpotential[0] = omegan**2/2 * norm2
    return np.sum(mom**2/2/m) + np.sum(potential) + np.sum(beadpotential)

def classical3(pos, mom, m, potential, force, numBeads, beta):
    # closed ring polymer
    # Can be used to check if energy is conserved.
    hbar = 1. # for now...
    omegan = numBeads/(beta*hbar)
    beadpotential = m*np.square(omegan)/2 * np.square(pos-np.roll(pos, 1, axis=0))
    #beadpotential = np.sum(beadpotential, axis=(1,2))
    #Eck = eckart_frame.EckartFrame(pos[-1], m.flatten())
    #xrot, rotmat, norm2, jac = Eck.norm2deriv(pos[0])
    #beadpotential[0] = omegan**2/2 * norm2
    return np.sum(mom**2/2/m) + np.sum(potential) + np.sum(beadpotential)

