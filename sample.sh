echo "Simple example, harmonic oscillator, Langevin thermostat"
python PIMD.py --beta 1.0 --numBeads 32 --numSteps 10000 --dt 0.01 -P '1Dharmonic' --thermostat 'Langevin' --printEne 1

