#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: simon

Classes for performing a PIMD simulation with a ring-polymer propagator.
"""

from __future__ import print_function, division
import math
import numpy as np
import random
from scipy.stats import sem

from polylib import md
from polylib import util
from polylib.PES import rp
from polylib import eckart_frame
import cyclic_NM


class RPpropagator(md.vv):
    r"""
    The unthermostatted PIMD ring-polymer propagator.
    """
    def __init__(self, PES, mass, dt, numBeads, beta, Xi, MTSfactor,
                 forceType=1, hbar=1):
        r"""
        Initialise the RPpropagator

        Parameters
        ----------
        PES : PES object (has member functions such as force() and potential()
            defined.)
            The PES on which a PIMD simulation is performed.

        m : float or ndarray of shape (M,) or (M, N)
            Atomic masses.

        dt : float
            timestep

        numBeads : integer
            number of beads

        beta : float
            beta = 1/kT

        Xi: float, [0, 1]
            Xi tells which Hamiltonian to use. The Hamiltonian to use is
            constructed as a linear combination between Hamtiltonians with a
            repulsive and attractive last spring, H = Xi*H_{repulsive} +
            (1-Xi)*H_{attractive}

        MTSfactor : int
            Used for the multi-time-step integrator. Tells how often the fast
            forces from the beads should be calculated compared to the slow
            forces from the PES.

        forceType : int
            Tells how the bead forces should be calculated. Depending on the
            value, a system is simulated which allows to calculate following
            partition function ratios:
            1: Z/Z
            2: Z(J=0)/Z
            3: Z(J=0)/Z(J=0)

        hbar : float, OPTIONAL
            hbar. Default 1.

        Returns
        -------
        None

        """
        rp_PES = rp.cart(numBeads, PES, mass, hbar, beta)
        super(RPpropagator, self).__init__(rp_PES, mass, dt)
        self.sqrtm = np.sqrt(mass)
        if (np.isscalar(self.m) is False):
            self.mflat = self.m.flatten()
        else:
            self.mflat = self.m
        self.mtot = np.sum(mass)
        self.n = numBeads
        self.beta = beta
        self.betaN = beta/numBeads
        self.hbar = hbar
        self.omegan = 1./self.betaN/hbar
        self.Xi = Xi
        self.MTSfactor = MTSfactor
        self.dtfast = self.dt/self.MTSfactor
        self.fType = forceType

    def set_x(self, x):
        super(RPpropagator, self).set_x(x)
        self.ffunRP()

    def ffun(self):
        self.Epot, self.f = self.PES.clPES.both(self.x)
        self.f *= -1

    def ffunRP(self):
        self.fRP = self.PES.fRP(self.x)
        if self.fType == 1:
            return
        elif self.fType == 2:
            self.ffunRPZJzeroZ()
        elif self.fType == 3:
            self.ffunRPZJzeroZJzero()

    def ffunRPZJzeroZ(self):
        self.fRP[-1] += self.Xi*self.m*self.omegan**2*(self.x[-1]-self.x[0])
        self.fRP[0] += self.Xi*self.m*self.omegan**2*(self.x[0]-self.x[-1])

        Eck = eckart_frame.EckartFrame(self.x[-1], self.mflat)
        xrot, rotmat, norm2, jac = Eck.norm2deriv(self.x[0])
        jac = jac.reshape((2, -1, 3))
        self.fRP[-1] -= self.Xi*(self.omegan**2)/2*jac[0]
        self.fRP[0] -= self.Xi*(self.omegan**2)/2*jac[1]

    def ffunRPZJzeroZJzero(self):
        # Open the closed ring-polymer
        self.fRP[-1] += self.m*self.omegan**2*(self.x[-1]-self.x[0])
        self.fRP[0] += self.m*self.omegan**2*(self.x[0]-self.x[-1])

        Eck = eckart_frame.EckartFrame(self.x[-1], self.mflat)
        xrot, rotmat, norm2, jac = Eck.norm2deriv(self.x[0])
        Eck2 = eckart_frame.EckartFrame(-self.x[-1], self.mflat)
        xrot2, rotmat2, norm22, jac2 = Eck2.norm2deriv(self.x[0])
        jac = jac.reshape((2, -1, 3))
        jac2 = jac2.reshape((2, -1, 3))
        self.fRP[-1] -= (1-self.Xi)*(self.omegan**2)/2*jac[0]
        self.fRP[0] -= (1-self.Xi)*(self.omegan**2)/2*jac[1]
        self.fRP[-1] += self.Xi*(self.omegan**2)/2*jac2[0]
        self.fRP[0] -= self.Xi*(self.omegan**2)/2*jac2[1]

    def step1(self):
        self.p += self.dt * self.f / 2
        for i in range(self.MTSfactor):
            self.p += self.dtfast * self.fRP / 2
            self.x += self.dtfast * self.p / self.m
            if self.fType != 1:
                com_pos = np.sum(self.x * self.m, axis=-2)/self.mtot
                self.x -= com_pos[..., None, :]
            self.ffunRP()
            self.p += self.dtfast * self.fRP / 2

    def step(self):
        self.step1()
        self.step2()
        if self.fType != 1:
            com_vel = np.sum(self.p, axis=-2)/self.mtot
            self.p -= com_vel[..., None, :]*self.m


class Andersen(RPpropagator):
    r"""
    PIMD propagator with Andersen thermostat. nu describes the probability with
    which momenta are resampled.
    """
    def __init__(self, PES, mass, dt, numBeads, beta, nu, Xi, MTSfactor,
                 forceType=1, hbar=1.):
        r"""
        Initialise the Andersen normal mode propagator

        Parameters
        ----------
        PES : PES object (has member functions such as force() and potential()
            defined.)
            The PES on which a PIMD simulation is performed.

        m : float or ndarray of shape (M,) or (M, N)
            Atomic masses.

        dt : float
            timestep

        numBeads : integer
            number of beads

        beta : float
            beta = 1/kT

        nu : float with a value between 0.0 and 1.0.
            Probability, with which momenta are resampled during every
            timestep.

        Xi: float, [0, 1]
            Xi tells which Hamiltonian to use. The Hamiltonian to use is
            constructed as a linear combination between Hamtiltonians with a
            repulsive and attractive last spring, H = Xi*H_{repulsive} +
            (1-Xi)*H_{attractive}

        MTSfactor : int
            Used for the multi-time-step integrator. Tells how often the fast
            forces from the beads should be calculated compared to the slow
            forces from the PES.

        forceType : int
            Tells how the bead forces should be calculated. Depending on the
            value, a system is simulated which allows to calculate following
            partition function ratios:
            1: Z/Z
            2: Z(J=0)/Z
            3: Z(J=0)/Z(J=0)


        hbar : float, OPTIONAL
            hbar. Default 1.

        Returns
        -------
        None

        """
        super(Anderson, self).__init__(PES, mass, dt, numBeads, beta=beta,
                                       Xi=Xi, MTSfactor=MTSfactor,
                                       forceType=forceType, hbar=hbar)
        self.nu = nu
        self.SD = np.sqrt(self.m/self.betaN)

    def rescale(self, definitely=False):
        if definitely or random.random() < self.nu:
            self.p = np.random.normal(0, self.SD, self.p.shape)

    def step2(self, s=1):
        RPpropagator.step2(self, s)
        self.rescale()


class Langevin(RPpropagator):
    r"""
    PIMD propagator with Langevin thermostat according to
    DOI: 10.1063/1.3489925. tau describes the coupling parameter for the
    lowest energy normal mode.
    """
    def __init__(self, PES, mass, dt, numBeads, beta, tau, Xi, MTSfactor,
                 forceType=1, hbar=1.):
        r"""
        Initialise the Langevin thermostatted normal mode propagator.

        Parameters
        ----------
        PES : PES object (has member functions such as force() and potential()
            defined.)
            The PES on which a PIMD simulation is performed.

        m : float or ndarray of shape (M,) or (M, N)
            Atomic masses.

        dt : float
            timestep

        numBeads : integer
            number of beads

        beta : float
            beta = 1/kT

        tau : float
            Inverse coupling strength of thermostat to centroid normal mode.

        Xi: float, [0, 1]
            Xi tells which Hamiltonian to use. The Hamiltonian to use is
            constructed as a linear combination between Hamtiltonians with a
            repulsive and attractive last spring, H = Xi*H_{repulsive} +
            (1-Xi)*H_{attractive}

        MTSfactor : int
            Used for the multi-time-step integrator. Tells how often the fast
            forces from the beads should be calculated compared to the slow
            forces from the PES.

        forceType : int
            Tells how the bead forces should be calculated. Depending on the
            value, a system is simulated which allows to calculate following
            partition function ratios:
            1: Z/Z
            2: Z(J=0)/Z
            3: Z(J=0)/Z(J=0)

        hbar : float, OPTIONAL
            hbar. Default 1.

        Returns
        -------
        None

        """
        super(Langevin, self).__init__(PES, mass, dt, numBeads, beta=beta,
                                       Xi=Xi, MTSfactor=MTSfactor,
                                       forceType=forceType, hbar=hbar)
        self.tau = tau
        self.nm = cyclic_NM.cyclic(numBeads)
        self.omegak = self.omegan * np.sqrt(self.nm.eigenvalues)
        self.gamma = np.where(self.omegak < 0.01, 1./tau, 2*self.omegak)
        self.c4 = np.exp(-dt/2*self.gamma)
        self.c5 = np.sqrt(1-self.c4**2)

    def step1(self):
        self.c_4 = util.backwardsbroadcast(self.c4, self.x)
        self.c_5 = util.backwardsbroadcast(self.c5, self.x)
        p_nm = self.nm.forward(self.p)
        p_nm[:] = (self.c_4*p_nm + np.sqrt(self.m/self.betaN) * self.c_5 *
                   np.random.normal(0.0, 1.0, size=np.shape(p_nm)))
        self.p = self.nm.backward(p_nm)
        RPpropagator.step1(self)

    def step2(self):
        self.c_4 = util.backwardsbroadcast(self.c4, self.x)
        self.c_5 = util.backwardsbroadcast(self.c5, self.x)
        RPpropagator.step2(self)  # (25)
        p_nm = self.nm.forward(self.p)  # (27)
        p_nm[:] = (self.c_4*p_nm + np.sqrt(self.m/self.betaN) * self.c_5 *
                   np.random.normal(0.0, 1.0, size=np.shape(p_nm)))  # (28)
        self.p = self.nm.backward(p_nm)
