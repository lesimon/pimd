#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: simon

Program to execute for performing PIMD simulations.
"""


from __future__ import division, print_function
import argparse
import datetime
import numpy as np
import os

from polylib import eckart_frame
from polylib import elements
from polylib import harmonic
from polylib import md
from polylib import units
from polylib import vmd
from polylib.PES import oneD
import cyclic_NM
import energyEstimator as ene
import NMpropagator as nmp
import RPpropagator as rp

parser = argparse.ArgumentParser(add_help=True)
temp = parser.add_mutually_exclusive_group()
time = parser.add_mutually_exclusive_group()
thermo_const = parser.add_mutually_exclusive_group()
temp.add_argument('--beta', default=1.0, type=float,
                  help="beta = 1/(Boltzmann constant * temperature) "
                       "(default=1.0, type=float, "
                       "mutually exclusive to --temp)")
temp.add_argument('--temp', default=-1, type=float,
                  help="temperature in Kelvin (type=float, "
                       "mutually exclusive to --beta)")
time.add_argument('--dt', '--timestep', default=0.02, type=float,
                  help="Time between subsequent steps of PIMD integration "
                  "(default=0.02, type=float)")
time.add_argument('--fs', default=-1, type=float,
                  help="timestep in femtoseconds (type=float, "
                  "mutually exclusive to --dt)")
thermo_const.add_argument('--nu', '--Nu', default=0.3, type=float,
                          help="Probability for scaling velocities in each "
                          "timestep when using the Andersen thermostat. "
                          "(default=0.3, type=float)")
thermo_const.add_argument('--tau', '--Tau', default=1., type=float,
                          help="Used for scaling the centroid mode in the "
                          "Langevin thermostat. The provided value should have"
                          " the units fs. (default=1.0, type=float)")
parser.add_argument('--dim', '--dimension', default=1, type=int,
                    help="Describes the dimension of PES if provided PES "
                    "works with arbitrary number of dimensions. (default=1, "
                    "type=int")
parser.add_argument('--eneEst', '--energyEstimator', default='Virial Est',
                    choices=['Virial Est', 'Centroid Virial Est',
                             'Classical Energy'],
                    help="Energy estimator for calculating average energies."
                    " (choices: 'Virial Est', 'Centroid Virial Est',"
                    " 'Classical Energy')")
parser.add_argument('--eneFile', default="eneFile.csv", help="Name "
                    "of file storing energies. (default=eneFile.csv)")
parser.add_argument('--eqSteps', default=0, type=int, help="Number of "
                    "equilibration steps to perform. It uses the same "
                    "thermostat as provided by --thermostat. (default=0, "
                    "type=int)")
parser.add_argument('--fep', default=0, type=int, help="Specifies to print "
                    "values required to perform free energy calculations at "
                    "every i-th step. If a value of zero is provided, nothing "
                    "is printed. (default=0, type=int)")
parser.add_argument('--fepFile', default='fep.txt', help="Name of file used to"
                    " store intermediate results of free energy perturbation.")
parser.add_argument('--hbar', default=1.0, type=float,
                    help="Numerical value of hbar (default=1.0, type=float)")
parser.add_argument('--inputFile', default=None, help="Name of the input file,"
                    " from which positions should be read-in.")
parser.add_argument('--MTSfactor', default=100, type=int,
                    help="How often the fast forces should be evaluated "
                    "compared to slow forces. Only used when PIMDtype='RP'. "
                    "(default=100, type=int).")
parser.add_argument('--numBeads', default=8, type=int,
                    help="Number of beeds which represent a simple particle "
                    "(default=8, type=int)")
parser.add_argument('--numSteps', default=10000, type=int,
                    help="Number of steps to perform in PIMD simulation "
                    "(default=10000, type=int)")
parser.add_argument('-P', '--PES', default='1Dharmonic',
                    choices=['1Dharmonic', 'nDharmonic', 'doubleWell',
                             'hydronium', 'qtip4pf'],
                    help="potential energy surface (choices: "
                    "'1Dharmonic', 'nDharmonic', 'doubleWell', 'hydronium',"
                    " qtip4pf)")
parser.add_argument('--PIMDtype', default='NM', choices=['NM', 'RP'],
                    help="The type of PIMD simulation to perform. NM = "
                    "uses a normal mode propagator, RP = uses ring polymer"
                    "propagator. (default='NM')")
parser.add_argument('--printEne', '--printEnergy', default=0, type=int,
                    help="Specifies to print energies at every i-th step. "
                    "If a value of zero is provided, no energies will be "
                    "printed to a file. (default=0, type=int)")
parser.add_argument('--printPos', default=0, type=int, help="Specifies "
                    "to print the positions at every i-th step. If "
                    "a value of zero is provided, no positions will be printed"
                    " to a file. (default=int, type=int)")
parser.add_argument('--printMom', default=0, type=int, help="Specifies "
                    "to print the momenta at every i-th step. If "
                    "a value of zero is provided, no momenta will be printed"
                    " to a file. (default=int, type=int)")
parser.add_argument('--ptFile', default="ptFile.xyz",
                    help="Name of the trajectory file containing positions. "
                    "(default=ptFile.xyz")
parser.add_argument('--seed', '--Seed', default=0, type=int,
                    help="Sets the seed for random number generators. "
                    "Currently used by the Andersen and Langevin thermostat.")
parser.add_argument('--thermostat', default='None',
                    choices=['None', 'Andersen', 'Langevin'],
                    help="The thermostat applied to keep the temperature "
                    "constant (default=None)")
parser.add_argument('--time', dest='time', action='store_true',
                    help="Enable free energy perturbation calculations.")
parser.add_argument('--Xi', '--xi', default=0.0, type=float,
                    help="The Xi value, at which the PIMD is performed. "
                    "(default=0.0, type=float)")
parser.add_argument('--xtFile', default="xtFile.xyz",
                    help="Name of the trajectory file containing "
                    "positions. (default=xtFile.xyz")
parser.set_defaults(time=False)

if __name__ == "__main__":
    args = parser.parse_args()
    Units = units.hartAng()
    np.random.seed(args.seed)
    forceType = 1
    # Currently, reading in files is only supported for the hydronium and
    # qtip4pf PES.
    if args.PES == '1Dharmonic':
        m = 1.
        w = 1.
        potential = oneD.harmonic(m, w)

        pos = np.zeros((args.numBeads, 1))
        mom = np.zeros((args.numBeads, 1))
        atomlist = args.PES
    elif args.PES == 'nDharmonic':
        m = 1.
        w = 1.
        potential = harmonic.MultiDimensional(omega=w, mass=m)
        f = args.dim
        pos = np.zeros((args.numBeads, f))
        mom = np.zeros((args.numBeads, f))
        atomlist = []
        for i in range(f):
            atomlist.append("nDharmonic " + str(i))
    elif args.PES == 'doubleWell':
        # Benderskii p 78
        m = 1.
        potential = oneD.double_well(lam=4, x0=1, kappa=0, offset=0, dip=0)
        pos = np.zeros((args.numBeads, 1))+0.4
        mom = np.zeros((args.numBeads, 1))
        atomlist = args.PES
    elif args.PES == 'hydronium':
        from polylib.PES import external
        potential = external.hydronium()
        if args.inputFile is None:
            args.inputFile = 'h3o+.xyz'
        ext = os.path.splitext(args.inputFile)[1]
        if ext == '.xyz':
            x, atomlist = vmd.load(args.inputFile, getatomlist=True)
        else:
            assert False, "Currently only .xyz files are supported!"
        m = elements.getmass(atomlist) * Units.amu
        if (x.shape == (4, 3)):
            pos = np.ones((args.numBeads, 4, 3)) * x
        else:
            pos = x[-args.numBeads:]
        # TODO: Add support for reading in momenta.
        mom = np.zeros(pos.shape)
        forceType = 3
    elif args.PES == 'qtip4pf':
        from polylib.PES import qtip4pf
        Units = units.kcalAamu()
        potential = qtip4pf.PES()
        if args.inputFile is None or 'None':
            args.inputFile = 'h2o.xyz'
        ext = os.path.splitext(args.inputFile)[1]
        if ext == '.xyz':
            x, atomlist = vmd.load(args.inputFile, getatomlist=True)
        else:
            assert False, "Currently only .xyz files are supported!"
        m = elements.getmass(atomlist) * Units.amu
        if (x.shape == (3, 3)):
            pos = np.ones((args.numBeads, 3, 3)) * x
        else:
            pos = x[-args.numBeads:]
        # TODO: Add support for reading in momenta.
        mom = np.zeros(pos.shape)
        forceType = 2

    if (args.temp > 0):
        args.beta = Units.betaTemp(args.temp)
    if (args.fs > 0):
        args.dt = args.fs / pow(10, 15) / Units.time

    if args.PIMDtype == 'NM':
        nm = cyclic_NM.open_cyclic(args.numBeads, args.Xi)
        if args.thermostat == 'None':
            prop = nmp.NMpropagator(potential, m, args.dt, args.numBeads,
                                    beta=args.beta, nm=nm, hbar=args.hbar)
        elif args.thermostat == 'Andersen':
            prop = nmp.Andersen(potential, m, args.dt, args.numBeads,
                                beta=args.beta, nu=args.nu, nm=nm,
                                hbar=args.hbar)
        elif args.thermostat == 'Langevin':
            # Convert tau from fs to a.t.u.
            args.tau *= (pow(10, 15) * Units.time)
            prop = nmp.Langevin(potential, m, args.dt, args.numBeads,
                                beta=args.beta, tau=args.tau, nm=nm,
                                hbar=args.hbar)
    elif args.PIMDtype == 'RP':
        if args.thermostat == 'None':
            prop = rp.RPpropagator(potential, m, args.dt, args.numBeads,
                                   beta=args.beta, Xi=args.Xi,
                                   MTSfactor=args.MTSfactor,
                                   forceType=forceType, hbar=args.hbar)
        elif args.thermostat == 'Andersen':
            prop = rp.Andersen(potential, m, args.dt, args.numBeads,
                               beta=args.beta, nu=args.nu, Xi=args.Xi,
                               MTSfactor=args.MTSfactor,
                               forceType=forceType, hbar=args.hbar)
        elif args.thermostat == 'Langevin':
            args.tau *= (pow(10, 15) * Units.time)
            prop = rp.Langevin(potential, m, args.dt, args.numBeads,
                               beta=args.beta, tau=args.tau, Xi=args.Xi,
                               MTSfactor=args.MTSfactor,
                               forceType=forceType, hbar=args.hbar)

    prop.set_xp(pos, mom)
    if args.time:
        a = datetime.datetime.now()
    if args.eqSteps > 0:
        for i in range(args.eqSteps):
            prop.step()
    if (args.printPos > 0):
        vmd.xyz(pos, atomlist, args.xtFile, extra="timestep 0")
    if (args.printMom > 0):
        vmd.xyz(mom, atomlist, args.ptFile, extra="timestep 0")
    if (args.printEne > 0):
        f = open(args.eneFile, 'w')
        if args.eneEst == 'Virial Est':
            est = ene.virialEstimator
        elif args.eneEst == 'Centroid Virial Est':
            est = ene.centroidVirialEstimator
        elif args.eneEst == 'Classical Energy':
            if args.PES == 'hydronium':
                est = ene.classicalH3O
            elif args.PES == 'qtip4pf':
                est = ene.classicalH2O
            else:
                est = ene.classical
        else:
            assert False, "Choice of energy estimator is invalid!"
        energy = est(prop.x, prop.p, m, prop.Epot, prop.f, args.numBeads,
                     args.beta, args.Xi, args.hbar)
        f.write(str(energy)+'\n')
    if (args.fep > 0):
        f2 = open(args.fepFile, 'w')
        betaN = args.beta / args.numBeads
        omegan = 1/betaN/args.hbar
        if args.PES == 'hydronium':
            mflat = m.flatten()
            Eck = eckart_frame.EckartFrame(prop.x[-1], mflat)
            norm2 = Eck.norm2deriv(prop.x[0])[2]
            Eck2 = eckart_frame.EckartFrame(-prop.x[-1], mflat)
            norm22 = Eck2.norm2deriv(prop.x[0])[2]
            fepTerm = -betaN*np.square(omegan)/2*(norm22 - norm2)
        elif args.PES == 'qtip4pf':
            mflat = m.flatten()
            Eck = eckart_frame.EckartFrame(prop.x[-1], mflat)
            norm2 = Eck.norm2deriv(prop.x[0])[2]
            c = betaN*np.square(omegan)/2
            fepTerm = -c*(norm2 - np.sum(m*np.square(prop.x[0] -
                                                     prop.x[-1])))
        elif args.PES == 'doubleWell':
            fepTerm = -betaN*np.square(omegan)*2*prop.x[0]*prop.x[-1]*m
            fepTerm = np.sum(fepTerm)
        f2.write(str(fepTerm)+'\n')
    for i in range(args.numSteps):
        prop.step()
        if (args.printPos > 0):
            if (i % args.printPos == 0):
                vmd.xyz(prop.x, atomlist, args.xtFile,
                        extra="timestep: "+str(i+1), append=True)
        if (args.printMom > 0):
            if (i % args.printMom == 0):
                vmd.xyz(prop.p, atomlist, args.ptFile,
                        extra="timestep: "+str(i+1), append=True)
        if (args.printEne > 0):
            if (i % args.printEne == 0):
                energy = est(prop.x, prop.p, m, prop.Epot, prop.f,
                             args.numBeads, args.beta, args.Xi, args.hbar)
                f.write(str(energy)+'\n')
        if (args.fep > 0):
            if (i % args.fep == 0):
                if args.PES == 'hydronium':
                    Eck = eckart_frame.EckartFrame(prop.x[-1], mflat)
                    norm2 = Eck.norm2deriv(prop.x[0])[2]
                    Eck2 = eckart_frame.EckartFrame(-prop.x[-1], mflat)
                    norm22 = Eck2.norm2deriv(prop.x[0])[2]
                    fepTerm = -betaN*np.square(omegan)/2*(norm22 - norm2)
                elif args.PES == 'qtip4pf':
                    Eck = eckart_frame.EckartFrame(prop.x[-1], mflat)
                    norm2 = Eck.norm2deriv(prop.x[0])[2]
                    c = betaN*np.square(omegan)/2
                    fepTerm = -c*(norm2 - np.sum(m*np.square(prop.x[0] -
                                                             prop.x[-1])))
                elif args.PES == 'doubleWell':
                    fepTerm = -betaN*np.square(omegan)*2*prop.x[0]*prop.x[-1]*m
                    fepTerm = np.sum(fepTerm)
                f2.write(str(fepTerm)+'\n')
    if args.time:
        print(datetime.datetime.now()-a)
    if (args.printEne > 0):
        f.close()
    if (args.fep > 0):
        f2.close()
